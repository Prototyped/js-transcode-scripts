var WshShell = new ActiveXObject ("WScript.Shell");
var WshEnv = WshShell.Environment("PROCESS");
WshEnv("HOME") = WshEnv("USERPROFILE");

var WshFso = new ActiveXObject ("Scripting.FileSystemObject");
var source = WScript.Arguments(0);
WScript.Echo(WshEnv("COMSPEC") + " /c dir /b \"" + source + "\"");
var WshExec = WshShell.Exec("%COMSPEC% /c dir /b \"" + source + "\"");
var target = WScript.Arguments(1);
var Filenames = new Array();
while (!WshExec.StdOut.AtEndOfStream)
{
	Filenames[Filenames.length] = WshExec.StdOut.ReadLine();
}

var RE = /\.[^.]+$/;
var skipRE = /\.std\.mp4$|\.exe$|\.idx$|\.srt$|\.ssa$|\.sub$|\.ass$|\.ifo$|\.nfo$|\.txt$|\.temp$|\.log$|\.mbtree$/i;
var audioRE = /Audio: ([^,]+), (\d+) Hz, ([^,]+), [^,]+, (\d+) kb\/s/m;

for (var key in Filenames)
{
	var height = 0, width = 0, length = 0, totalBitrate = 0, origSize = 0;
	if (Filenames[key].match (skipRE))
		continue;
    if (WshFso.FileExists (Filenames[key].replace (RE, ".std.mp4"))) {
        WScript.Echo ("Regularized file already exists, skipping " + Filenames[key]);
        continue;
    }
	WScript.Echo ("Working on " + Filenames[key]);
	try
	{
		var wmp = new ActiveXObject ("WMPlayer.OCX.7");
		wmp.windowlessVideo = 1;
		wmp.URL = source + "\\" + Filenames[key];
		
		var retry = 0;
		var media;
		do
		{
			WScript.Sleep(50);
			++retry;
			if (wmp.openState == 13)
			{
				media = wmp.currentMedia;
				height = media.imageSourceHeight;
				width = media.imageSourceWidth;
			}
		} while ((wmp.openState != 13 || height == 0 || width == 0)
				&& retry < 60000/50);
		if (retry >= 60000/50)
		{
			WScript.Echo ("ERROR: too many retries while waiting for media to be ready");
			continue;
		}

		length = media.duration;
		var mediaFile = WshFso.GetFile(Filenames[key]);
		// origSize = media.getItemInfo("FileSize");
		origSize = mediaFile.size;
		totalBitrate = media.getItemInfo("Bitrate");
		WScript.Echo ("Info: " + Filenames[key] + " w " + width + "; h " + height + "; l " + length + " bitrate " + totalBitrate);
		if (totalBitrate == "")
		{
			totalBitrate = Number.MAX_VALUE;
		}
		wmp.close ();
	}
	catch (e)
	{
		WScript.Echo ("ERROR: File " + Filenames[key] + ": WMP exception: " + e.description);
		if (wmp != null)
		{
			wmp.close ();
		}
		continue;
	}
		
	if (height == 0 || width == 0)
	{
		WScript.Echo ("ERROR: File " + Filenames[key] + " has unknown size.");
		continue;
	}

	if (width * height > 854 * 480)
	{
		newHeight = 16 * Math.round (Math.sqrt (854 * 480 * height / width) / 16);
		newWidth = 16 * Math.round (Math.sqrt (854 * 480 * width / height) / 16);
	}
	else
	{
		newHeight = 16 * Math.round (height / 16);
		newWidth = 16 * Math.round (width / 16);
	}

	var bitrate = Math.ceil (0.53013 * Math.pow (newWidth * newHeight, 0.6));
	WScript.Echo ("Projected lengths: old " + origSize + "; new " + (bitrate / 8 + 16) * length * 1000);
	// 15 megs are fine
if ((totalBitrate > bitrate * 1000 + 128000 || newHeight <= height && newWidth <= width) && origSize > (bitrate / 8 + 16) * length * 1000 + 15728640)
	{
		WScript.Echo (Filenames[key] + " has a higher bitrate, processing.");
		WScript.Echo ("ffmpeg -y -i \"" + source + "\\" + Filenames[key] + "\" -an -vcodec libx264 -preset medium -s " + newWidth + "x" + newHeight + " -pass 1 -threads 0 -b:v " + bitrate + "k -bt " + bitrate + "k -bf 16 \"" + target + "\\" + Filenames[key].replace(RE, ".std.mp4") + "\"");
		var exitCode = WshShell.Run ("%COMSPEC% /c ffmpeg -y -i \"" + source + "\\" + Filenames[key] + "\" -an -vcodec libx264 -preset medium -s " + newWidth + "x" + newHeight + " -pass 1 -threads 0 -b:v " + bitrate + "k -bt " + bitrate + "k -bf 16 \"" + target + "\\" + Filenames[key].replace(RE, ".std.mp4") + "\" > \"" + Filenames[key].replace(RE, ".out.txt") + "\" 2>&1", 0, true);
//		WScript.Echo ("ffmpeg -y -i \"" + source + "\\" + Filenames[key] + "\" -an -vcodec libx264 -vpre fastfirstpass -s " + newWidth + "x" + newHeight + " -pass 1 -threads 0 -b " + bitrate + "k -bt " + bitrate + "k -bf 16 \"" + target + "\\" + Filenames[key].replace(RE, ".std.mp4") + "\"");
//		var exitCode = WshShell.Run ("%COMSPEC% /c ffmpeg -y -i \"" + source + "\\" + Filenames[key] + "\" -an -vcodec libx264 -vpre fastfirstpass -s " + newWidth + "x" + newHeight + " -pass 1 -threads 0 -b " + bitrate + "k -bt " + bitrate + "k -bf 16 \"" + target + "\\" + Filenames[key].replace(RE, ".std.mp4") + "\" > \"" + Filenames[key].replace(RE, ".out.txt") + "\" 2>&1", 0, true);
		var audioBitrate = 0, sampleRate = 0, channelString, output = "";
		var logFile = WshFso.GetFile (Filenames[key].replace(RE, ".out.txt"));
		var stream = logFile.OpenAsTextStream (1, 0);
		while (!stream.AtEndOfStream)
		{
			var ffmpegLog = stream.ReadAll ();
			output += ffmpegLog ;
		}
		stream.Close ();
		logFile.Delete ();
		var match;
		if ((match = audioRE.exec (output)) != null)
		{
			audioBitrate = parseInt(match [4]);
			sampleRate = parseInt(match [2]);
			channelString = match [3];
		}

		WScript.Echo (Filenames[key] + " has audio with channels " + channelString + ", sample rate " + sampleRate + " and bitrate " + audioBitrate);

		var channels = 2;
		var audioBitRate = 128;
		if (exitCode == 0)
		{
			if (channelString == "mono") {
				audioBitrate = 64;
				channels = 1;
			}
			else if (channelString.substr(0, 3) == "5.1") {
				channels = 6;
				audioBitrate = 192;
			}


			WScript.Echo ("ffmpeg -y -i \"" + source + "\\" + Filenames[key] + "\" -acodec libfdk_aac -profile:a aac_he -cutoff 18000 -ac " + channels.toString() + " -ab " + audioBitrate.toString () + "k -vcodec libx264 -preset medium -s " + newWidth + "x" + newHeight + " -pass 2 -threads 0 -b:v " + bitrate + "k -bt " + bitrate + "k -bf 16 \"" + target + "\\" + Filenames[key].replace(RE, ".std.mp4") + "\"");
			WshShell.Run ("ffmpeg -y -i \"" + source + "\\" + Filenames[key] + "\" -acodec libfdk_aac -profile:a aac_he -cutoff 18000 -ac " + channels.toString() + " -ab " + audioBitrate.toString ()  + "k -vcodec libx264 -preset medium -s " + newWidth + "x" + newHeight + " -pass 2 -threads 0 -b:v " + bitrate + "k -bt " + bitrate + "k -bf 16 \"" + target + "\\" + Filenames[key].replace(RE, ".std.mp4") + "\"", 0, true);
		}
	}
	else
	{
		WScript.Echo (Filenames[key] + " already has a low bitrate, skipping.");
	}
}
