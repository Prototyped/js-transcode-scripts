var WshShell = new ActiveXObject ("WScript.Shell");
var WshEnv = WshShell.Environment ("PROCESS");
WshEnv ("HOME") = WshEnv ("USERPROFILE");

var WshFso = new ActiveXObject ("Scripting.FileSystemObject");
var WshExec = WshShell.Exec("%COMSPEC% /c dir /b " + WScript.Arguments(0));
var Filenames = new Array();
while (!WshExec.StdOut.AtEndOfStream)
{
	Filenames[Filenames.length] = WshExec.StdOut.ReadLine();
}


var RE = /\.[^.]+$/;
var aviRE = /\.avi$/i;
var mp4RE = /\.(mp4|mov|flv)$/i;

var fileRE = /(\d+) x (\d+)/;
var mp4boxRE = /Visual Size (\d+) x (\d+) /;

for (var key in Filenames)
{
	var height = 0, weight = 0;
	var sizeCmd, sizeRE;
	if (Filenames[key].match (aviRE))
	{
		sizeCmd = "file \"" + Filenames[key] + "\"";
		sizeRE = fileRE;
	}
	else if (Filenames[key].match (mp4RE))
	{
		sizeCmd = "MP4Box -info \"" + Filenames[key] + "\"";
		sizeRE = mp4boxRE;
	}
	else
	{
		WScript.Echo ("ERROR: File " + Filenames[key] + " has unknown suffix.");
		continue;
	}
	WshExec = WshShell.Exec(sizeCmd);
	while (!WshExec.StdOut.AtEndOfStream)
	{
		var matches;
		if ((matches = WshExec.StdOut.ReadLine().match (sizeRE)) != null)
		{
			width = matches [1];
			height = matches [2];
			while (!WshExec.StdOut.AtEndOfStream)
				WshExec.StdOut.ReadLine();
		}
	}
	if (height == 0 || width == 0)
	{
		WScript.Echo ("ERROR: File " + Filenames[key] + " has unknown size.");
		continue;
	}


	if (3 * width > 4 * height)
	{
		newWidth = 320;
		newHeight = 16 * Math.round (20 * height / width);
	}
	else
	{
		newHeight = 240;
		newWidth = 16 * Math.round (15 * width / height);
	}

	if (WshShell.Run ("ffmpeg -y -i \"" + Filenames[key] + "\" -an -vcodec libx264 -vpre wildfire -s " + newWidth + "x" + newHeight + " -pass 1 -threads 0 -b 288k -bt 32k \"" + Filenames[key].replace(RE, ".wildfire.mp4") + "\"", 0, true) == 0)
	{
		if (WshShell.Run ("ffmpeg -y -i \"" + Filenames[key] + "\" -acodec libfaac -ab 128k -ac 2 -vcodec libx264 -vpre wildfire -s " + newWidth + "x" + newHeight + " -pass 2 -threads 0 -b 288k -bt 32k \"" + Filenames[key].replace(RE, ".wildfire.mp4") + "\"", 0, true) == 0) {
			WshShell.Run ("MP4Box -isma \"" + Filenames[key].replace(RE, ".wildfire.mp4") + "\"", 0, true);
		}
	}
}
